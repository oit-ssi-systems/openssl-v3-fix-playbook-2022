# Upgrade OpenSSL to v3.0.7 

Verify the package URLs exist in the playbook. We don't actually know these
yet, but hopefully will on 11/1

Verify ansible-core is installed on the host running the playbook.

```
## For ubuntu
$ apt install ansible-core -y
$ ansible-playbook -i your-host-inventory.txt ./fix_ubuntu.yaml
## Or for el9
$ dnf install ansible-core -y
$ ansible-playbook -i your-host-inventory.txt ./fix_el9.yaml
```

Note that for the `el9` playbook, we are pulling the update from AlmaLinux. You
may need to tweak this to your official el9 flavor (Like CentOS, RedHat, etc).

Test first and all that of course 😁
